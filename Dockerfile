FROM node:16-alpine3.11

WORKDIR /app

COPY server.js .

RUN npm i express

RUN mkdir build

COPY ./dist/angular-admin-client ./build

EXPOSE 80

CMD ["node", "server"]