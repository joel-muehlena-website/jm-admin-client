import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotFound404Component } from './not-found404/not-found404.component';
import { NoPermission403Component } from './no-permission403/no-permission403.component';
import { AddAuthHeaderInterceptor } from './add-auth-header.interceptor';
import { GetNewAccessTokenInterceptor } from './get-new-access-token.interceptor';

import { MarkdownModule } from 'ngx-markdown';
import { AuthWrapperComponent } from './auth-wrapper/auth-wrapper.component';
import { UserIconComponent } from './user-icon/user-icon.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    DashboardComponent,
    NotFound404Component,
    NoPermission403Component,
    AuthWrapperComponent,
    UserIconComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MarkdownModule.forRoot(),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AddAuthHeaderInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: GetNewAccessTokenInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
