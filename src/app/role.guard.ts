import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AuthService } from './auth.service';

import * as base64 from 'base-64';

/**
 * An Angular Guard for securing role sensitive paths
 */
@Injectable({
  providedIn: 'root',
})
export class RoleGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  /**
   * Method gets called by the guard mechanism of Anfular
   * Needs the data.requiredPermission of a route to be set with <permission name>:<operation name> e.g. about:get
   * @param {ActivatedRouteSnapshot} route A snapshot of the activated route
   * @returns {Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree} A value which indicates the router if the user has the right role
   */
  canActivate(
    route: ActivatedRouteSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    //if no requiredPermission data present do not allow
    if (!route.data.requiredPermission) {
      return false;
    }

    //else check the role permission with the auth server
    return new Observable<boolean>((observer) => {
      this.authService
        .validateRolePermission(route.data.requiredPermission)
        .pipe(
          catchError((err) => {
            this.router.navigate(['/403'], {
              queryParams: {
                reason: base64.encode(
                  `Not enough rights to access ${
                    route.data.requiredPermission.split(':')[0]
                  }`
                ),
              },
            });
            observer.next(false);
            throw err;
          })
        )
        .subscribe((data) => {
          if (data.code === 200) {
            observer.next(true);
          } else {
            this.router.navigate(['/403'], {
              queryParams: {
                reason: base64.encode(
                  `Not enough rights to access ${
                    route.data.requiredPermission.split(':')[0]
                  }`
                ),
              },
            });
            observer.next(false);
          }
        });
    });
  }
}
