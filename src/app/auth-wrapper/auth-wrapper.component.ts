import { Component } from '@angular/core';
import { Subject } from 'rxjs';
import { SidebarControlEvent } from '../header/header.component';
import { Layout, LayoutService } from '../layout.service';
import { MainNavigationEntrySection } from '../main-navigation-entry';

@Component({
  selector: 'app-auth-wrapper',
  templateUrl: './auth-wrapper.component.html',
  styleUrls: ['./auth-wrapper.component.sass'],
})
export class AuthWrapperComponent {
  layout: Layout = 'Desktop';

  sidebarControlEvent = new Subject<SidebarControlEvent>();

  public readonly navigationEntries: Array<MainNavigationEntrySection> = [
    {
      points: [
        {
          name: 'Dashboard',
          path: '/dashboard',
          enableRouterLinkActive: true,
          routerLinkActiveOptions: { exact: true },
        },
      ],
    },
    {
      name: 'Homepage',
      points: [
        {
          name: 'About',
          path: '/homepage/about',
          enableRouterLinkActive: true,
        },
        {
          name: 'Skills',
          path: '/homepage/skills',
          enableRouterLinkActive: true,
        },
        {
          name: 'Education',
          path: '/homepage/education',
          enableRouterLinkActive: true,
        },
        {
          name: 'Experiences',
          path: '/homepage/experiences',
          enableRouterLinkActive: true,
        },
        {
          name: 'Projects',
          path: '/homepage/projects',
          enableRouterLinkActive: true,
        },
      ],
    },
  ];

  constructor(private layoutService: LayoutService) {
    this.layoutService.startWatching();

    this.layoutService.layout.subscribe((layout) =>
      this.onLayoutChange(layout)
    );
  }

  private onLayoutChange(layout: Layout) {
    this.layout = layout;
  }

  onSidebarControlEvent(e: SidebarControlEvent) {
    this.sidebarControlEvent.next(e);
  }
}
