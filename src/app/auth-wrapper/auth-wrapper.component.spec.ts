import { DebugElement } from '@angular/core';
import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import {
  HeaderComponent,
  SidebarControlEvent,
} from '../header/header.component';
import { LayoutService } from '../layout.service';
import { SidebarComponent } from '../sidebar/sidebar.component';

import { AuthWrapperComponent } from './auth-wrapper.component';

describe('AuthWrapperComponent', () => {
  let component: AuthWrapperComponent;
  let fixture: ComponentFixture<AuthWrapperComponent>;
  let debugElement: DebugElement;
  let layoutService: LayoutService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [AuthWrapperComponent, HeaderComponent, SidebarComponent],
    }).compileComponents();
    layoutService = TestBed.inject(LayoutService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthWrapperComponent);
    component = fixture.componentInstance;
    debugElement = fixture.debugElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle a SidebarControlEvent onClick on the header btn', fakeAsync(() => {
    const fakeEvent: SidebarControlEvent = {
      layout: 'Desktop',
      state: false,
      type: 'expand',
    };

    spyOn(component, 'onSidebarControlEvent').and.callFake(() => {
      return fakeEvent;
    });

    const el = debugElement.query(By.css('.jmAdminClient__headerToggleBtn'));

    expect(el).not.toBeNull();

    component.sidebarControlEvent.subscribe((e) => {
      expect(e).toBe(fakeEvent);
    });

    el.nativeElement.click();
    tick();
    fixture.detectChanges();

    expect(component.onSidebarControlEvent).toHaveBeenCalledWith(fakeEvent);
  }));

  it('should detect a layout change', fakeAsync(() => {
    component.layout = 'Desktop';
    expect(component.layout).toBe('Desktop');
    layoutService.layout.next('Mobile');

    tick();
    fixture.detectChanges();

    expect(component.layout).toBe('Mobile');
  }));
});
