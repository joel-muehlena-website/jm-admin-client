import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class AddAuthHeaderInterceptor implements HttpInterceptor {
  constructor(private authSevice: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    if (this.authSevice.accessToken) {
      const authReq = request.clone({
        headers: request.headers.set(
          'Authorization',
          this.authSevice.accessToken
        ),
        withCredentials: true,
      });
      return next.handle(authReq);
    } else {
      const authReq = request.clone({
        withCredentials: true,
      });
      return next.handle(authReq);
    }
  }
}
