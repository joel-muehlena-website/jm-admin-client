import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { AddAuthHeaderInterceptor } from './add-auth-header.interceptor';

describe('AddAuthHeaderInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AddAuthHeaderInterceptor],
    })
  );

  it('should be created', () => {
    const interceptor: AddAuthHeaderInterceptor = TestBed.inject(
      AddAuthHeaderInterceptor
    );
    expect(interceptor).toBeTruthy();
  });
});
