import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-user-icon',
  templateUrl: './user-icon.component.html',
  styleUrls: ['./user-icon.component.sass'],
})
export class UserIconComponent implements OnInit, AfterViewInit {
  /** users avatar value */
  avatar: string = '';

  /** flag if the user has an avatar */
  hasAvatar: boolean = false;

  /** internal var of the icon size */
  private _size: string = '8rem';

  @ViewChild('userIconRef') userIconRef!: ElementRef<HTMLDivElement>;

  /**
   * Set the size for the user icon
   * @param {string} value The size for the icon
   */
  @Input('size')
  set size(value: string) {
    this._size = value;
    this.setIconSize();
  }

  /**
   * Get the current icon size
   * @returns {string} The size if the icon
   */
  get size(): string {
    return this._size;
  }

  /**
   * Constructs component
   * @param authService The AuthService for getting the avatar
   */
  constructor(private authService: AuthService) {}

  /**
   * Inits the component and fetches or creates avatar
   */
  ngOnInit(): void {
    const user = this.authService.user;

    if (user && user.avatar && user.avatar.length > 0) {
      this.avatar = user.avatar;
      this.hasAvatar = true;
    } else if (user) {
      this.avatar = user.firstName.charAt(0) + user.lastName.charAt(0);
    }
  }

  ngAfterViewInit() {
    this.setIconSize();
  }

  private setIconSize() {
    if (this.userIconRef) {
      if (this.hasAvatar) {
        const userIcon = this.userIconRef.nativeElement.querySelector(
          '.userIcon__icon'
        ) as HTMLElement;
        userIcon.style.width = this.size;
        userIcon.style.height = this.size;
      }

      if (!this.hasAvatar) {
        const userIconWrapper: HTMLElement | undefined =
          this.userIconRef.nativeElement.querySelector(
            '.userIcon__iconWrapper--noAvatar'
          ) as HTMLElement;
        if (userIconWrapper !== undefined) {
          userIconWrapper.style.width = this.size;
          userIconWrapper.style.height = this.size;
        }
      }
    }
  }
}
