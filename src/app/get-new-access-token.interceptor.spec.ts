import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { GetNewAccessTokenInterceptor } from './get-new-access-token.interceptor';

describe('GetNewAccessTokenInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GetNewAccessTokenInterceptor],
    })
  );

  it('should be created', () => {
    const interceptor: GetNewAccessTokenInterceptor = TestBed.inject(
      GetNewAccessTokenInterceptor
    );
    expect(interceptor).toBeTruthy();
  });
});
