import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  AfterViewInit
} from '@angular/core';
import { Layout } from '../layout.service';

export interface SidebarControlEvent {
  type: 'expand' | 'open';
  layout: Layout;
  state: boolean;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements AfterViewInit {
  /** internal variable for the layout just for state holding. The job is done by getter and setter */
  private _layout: Layout = 'Desktop';

  /** Ref fot the HTML header el */
  @ViewChild('headerRef') headerRef!: ElementRef<HTMLDivElement>;

  /** gets the layout from its parent. Prevents from subscribing to layoutService in every component */
  @Input('layout')
  set layout(layout: Layout) {
    this.calculateSidebarFlag(layout, this._layout);
    this._layout = layout;
  }
  /**
   * Gets the current layout
   * @returns {Layout} The current layout
   */
  get layout(): Layout {
    return this._layout;
  }

  /** EventEmitter for an sidebar change event. */
  @Output() sidebarControlEvent = new EventEmitter<SidebarControlEvent>();

  /**
   * Flag for the sidebar state for both mobile and desktop layout.
   * Its treated differently for each layout
   */
  sidebarControlFlag = false;

  /** Called after view innited calculate flags and set initial classes for header width  */
  ngAfterViewInit(): void {
    //Calculate flags on init because the setter way doesn't work
    this.calculateSidebarFlag(
      this._layout,
      this._layout === 'Desktop' ? 'Mobile' : 'Desktop'
    );
  }

  /**
   * Calculate the initial flag state for a new layout. Only calculate if the layout has changed
   * @param {Layout} newLayout The new layout type
   * @param {Layout} prevLayout The old layout type
   */
  private calculateSidebarFlag(newLayout: Layout, prevLayout: Layout) {
    if (newLayout === 'Mobile' && prevLayout !== 'Mobile') {
      this.sidebarControlFlag = false;

      if (this.headerRef !== undefined) {
        this.headerRef.nativeElement.classList.add('hundred');
        this.headerRef.nativeElement.classList.remove('not-fullsize');
      }
    } else if (newLayout === 'Desktop' && prevLayout !== 'Desktop') {
      this.sidebarControlFlag = true;
      if (this.headerRef !== undefined) {
        this.headerRef.nativeElement.classList.remove('hundred');
      }
    }
  }

  /**
   * Toggle the sidebar or open the mobile nav depends on layout
   */
  toggleSidebar() {
    const event: SidebarControlEvent = {
      layout: this.layout,
      state: !this.sidebarControlFlag,
      type: this.layout === 'Desktop' ? 'expand' : 'open',
    };

    this.sidebarControlEvent.emit(event);

    if (this.layout === 'Desktop') {
      this.sidebarControlFlag = !this.sidebarControlFlag;

      if (this.headerRef !== undefined) {
        this.headerRef.nativeElement
          .querySelector('.jmAdminClient__headerToggleBtn i')
          ?.classList.toggle('rotate');

        this.headerRef.nativeElement.classList.toggle('not-fullsize');
      }
    }
  }

  /**
   * Handles a click on the user icon
   */
  onUserIconClick() {}
}
