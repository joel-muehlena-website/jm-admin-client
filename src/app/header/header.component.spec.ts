import {
  ComponentFixture,
  fakeAsync,
  TestBed,
  tick,
} from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HeaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should recalculate sidebarflag on layout change', () => {
    component['_layout'] = 'Desktop';
    expect(component.layout).toBe('Desktop');

    spyOn<any>(component, 'calculateSidebarFlag').and.callThrough();
    component.layout = 'Mobile';

    expect(component.layout).toBe('Mobile');
    expect(component['calculateSidebarFlag']).toHaveBeenCalledWith(
      'Mobile',
      'Desktop'
    );
    expect(component.sidebarControlFlag).toBeFalse();
  });

  it('should not recalculate sidebarflag on layout change if layout type is the same', () => {
    component['_layout'] = 'Desktop';
    expect(component.layout).toBe('Desktop');

    spyOn<any>(component, 'calculateSidebarFlag').and.callThrough();
    component.layout = 'Mobile';

    expect(component.layout).toBe('Mobile');
    expect(component['calculateSidebarFlag']).toHaveBeenCalledWith(
      'Mobile',
      'Desktop'
    );
    expect(component.sidebarControlFlag).toBeFalse();
    //Change state and recall with same layout type
    component.sidebarControlFlag = true;

    component.layout = 'Mobile';
    expect(component.layout).toBe('Mobile');
    expect(component['calculateSidebarFlag']).toHaveBeenCalledWith(
      'Mobile',
      'Mobile'
    );
    expect(component.sidebarControlFlag).toBeTrue();
  });

  it('should have sidebarFlag default to be false with mobile layout', () => {
    component.layout = 'Mobile';
    expect(component.layout).toBe('Mobile');
    expect(component.sidebarControlFlag).toBeFalse();
  });

  it('should have sidebarFlag default to be true with desktop layout', () => {
    component.layout = 'Desktop';
    expect(component.layout).toBe('Desktop');
    expect(component.sidebarControlFlag).toBeTrue();
  });

  it('should toggle expand sidebar event in desktop layout', fakeAsync(() => {
    component.layout = 'Desktop';
    expect(component.layout).toBe('Desktop');
    expect(component.sidebarControlFlag).toBeTrue();

    const currFlagVal = component.sidebarControlFlag;

    const el = fixture.debugElement.query(
      By.css('.jmAdminClient__headerToggleBtn')
    );
    expect(el).not.toBeNull();

    spyOn(component, 'toggleSidebar').and.callThrough();

    component.sidebarControlEvent.subscribe((e) => {
      expect(e).toEqual({
        layout: 'Desktop',
        state: !currFlagVal,
        type: 'expand',
      });
    });

    el.nativeElement.click();
    tick();
    fixture.detectChanges();

    expect(component.sidebarControlFlag).toBe(!currFlagVal);
    expect(component.toggleSidebar).toHaveBeenCalledTimes(1);
  }));

  it('should toggle open sidebar event in mobile layout', fakeAsync(() => {
    component.layout = 'Mobile';
    expect(component.layout).toBe('Mobile');
    expect(component.sidebarControlFlag).toBeFalse();

    const currFlagVal = component.sidebarControlFlag;

    const el = fixture.debugElement.query(
      By.css('.jmAdminClient__headerToggleBtn')
    );
    expect(el).not.toBeNull();

    spyOn(component, 'toggleSidebar').and.callThrough();

    component.sidebarControlEvent.subscribe((e) => {
      expect(e).toEqual({
        layout: 'Mobile',
        state: !currFlagVal,
        type: 'open',
      });
    });

    el.nativeElement.click();
    tick();
    fixture.detectChanges();

    expect(component.sidebarControlFlag).toBe(!currFlagVal);
    expect(component.toggleSidebar).toHaveBeenCalledTimes(1);
  }));
});
