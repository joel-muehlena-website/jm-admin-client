import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from './user';

export interface GetAccessTokenResponse {
  status?: number;
  token?: string;
  error?: string;
}

export interface DefaultApiResponse {
  code: number;
  msg: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _user: User | undefined = undefined;
  private _accessToken: string | undefined = undefined;

  constructor(private http: HttpClient) {}

  public set user(userData: User | undefined) {
    this._user = userData;
  }

  public get user(): User | undefined {
    return this._user;
  }

  public set accessToken(userData: string | undefined) {
    this._accessToken = userData;
  }

  public get accessToken(): string | undefined {
    return this._accessToken;
  }

  public fetchAccessToken(): Observable<GetAccessTokenResponse> {
    return this.http.get<GetAccessTokenResponse>(
      `${environment.authServer}/auth/token`
    );
  }

  public validateToken(): Observable<DefaultApiResponse> {
    return this.http.get<DefaultApiResponse>(
      `${environment.authServer}/auth/verifyToken`
    );
  }

  public validateRolePermission(permission: string) {
    if (this._user?.roles) {
      const body = new URLSearchParams();
      body.append('permission', permission);
      body.append('roleName', this._user?.roles);

      return this.http.post<DefaultApiResponse>(
        `${environment.authServer}/auth/verifyRole`,
        body,
        {
          headers: new HttpHeaders().set(
            'Content-Type',
            'application/x-www-form-urlencoded'
          ),
        }
      );
    } else {
      return of({ msg: 'No user loggedin', code: 403 });
    }
  }

  public logout(): Observable<DefaultApiResponse> {
    return this.http.get<DefaultApiResponse>(
      `${environment.authServer}/auth/logout`
    );
  }

  public isLoggedIn(): boolean {
    if (this.user === undefined) {
      return false;
    }
    return true;
  }
}
