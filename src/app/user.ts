/**
 * Describes the user data which is provided by the JWT of the auth server
 */
export interface User {
  /** the type of the token refresh or access */
  type: string;
  /** the user id */
  id: string;
  /** the role of the user */
  roles: string;
  /** the users username */
  username: string;
  /** the users firstname */
  firstName: string;
  /** the users lastname */
  lastName: string;
  /** the url of the users avatar */
  avatar: string;
  /** when the token was issued */
  iat: number;
  /** when the token expires */
  exp: number;
}
