export interface MainNavigationEntry {
  name: string;
  path: string;
  enableRouterLinkActive?: boolean;
  routerLinkActiveOptions?: { exact: boolean };
}

export interface MainNavigationEntrySection {
  name?: string;
  points: Array<MainNavigationEntry>;
}
