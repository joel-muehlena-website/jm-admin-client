import { Component, OnDestroy } from '@angular/core';
import { LayoutService } from './layout.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent implements OnDestroy {
  constructor(private layoutService: LayoutService) {
    layoutService.startWatching();
  }

  ngOnDestroy() {
    this.layoutService.stopWatching();
  }
}
