import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { NoPermission403Component } from './no-permission403.component';

describe('NoPermission403Component', () => {
  let component: NoPermission403Component;
  let fixture: ComponentFixture<NoPermission403Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [NoPermission403Component],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoPermission403Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
