import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import * as base64 from 'base-64';

@Component({
  selector: 'app-no-permission403',
  templateUrl: './no-permission403.component.html',
  styleUrls: ['./no-permission403.component.sass'],
})
export class NoPermission403Component implements OnInit {
  constructor(private route: ActivatedRoute) {}

  reason: string | undefined = undefined;

  ngOnInit(): void {
    this.route.queryParamMap.subscribe((params) => {
      const rs = params.get('reason');
      if (rs) {
        this.reason = base64.decode(rs);
      }
    });
  }
}
