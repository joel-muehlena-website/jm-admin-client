import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from './auth.service';

import { RoleGuard } from './role.guard';

import { name, internet } from 'faker';
import { User } from './user';

describe('RoleGuard', () => {
  let guard: RoleGuard;
  let authService: AuthService;
  let fakeUrls = ['', '/homepage/about', '/homepage/skills'];
  let fakeUser: User = {
    avatar: internet.avatar(),
    exp: new Date().getTime() + 20000,
    iat: new Date().getTime(),
    firstName: name.firstName(undefined),
    lastName: name.lastName(),
    id: '1',
    role: 'admin',
    type: 'access',
    username: internet.userName(),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
    });
    guard = TestBed.inject(RoleGuard);
    authService = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
