import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import jwtDecode from 'jwt-decode';
import { AuthService } from './auth.service';

@Injectable()
export class GetNewAccessTokenInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService) {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      tap((evt) => {
        if (
          evt instanceof HttpResponse &&
          evt.ok &&
          evt.headers.has('newAccessToken')
        ) {
          this.authService.accessToken = evt.headers.get('newAccessToken')!;
          this.authService.user = jwtDecode(
            evt.headers.get('newAccessToken')!
          ) as any;
        }
      })
    );
  }
}
