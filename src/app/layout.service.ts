import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export type Layout = 'Mobile' | 'Desktop';

@Injectable({
  providedIn: 'root',
})
export class LayoutService {
  private $layoutType: BehaviorSubject<Layout> = new BehaviorSubject<Layout>(
    'Desktop'
  );

  private isWatching: boolean = false;

  private readonly MOBILE_BREAKPOINT = 950;

  constructor() {
    this.onResize = this.onResize.bind(this);
    this.onResize();
  }

  startWatching() {
    if (!this.isWatching) {
      this.isWatching = true;
      window.addEventListener('resize', this.onResize);
    }
  }

  stopWatching() {
    if (this.isWatching) {
      this.isWatching = false;
      window.removeEventListener('resize', this.onResize);
    }
  }

  private onResize() {
    if (window.innerWidth < this.MOBILE_BREAKPOINT) {
      this.$layoutType.next('Mobile');
    } else {
      this.$layoutType.next('Desktop');
    }
  }

  public get layout(): BehaviorSubject<Layout> {
    return this.$layoutType;
  }
}
