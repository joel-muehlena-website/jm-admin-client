import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthWrapperComponent } from './auth-wrapper/auth-wrapper.component';
import { AuthGuard } from './auth.guard';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NoPermission403Component } from './no-permission403/no-permission403.component';
import { NotFound404Component } from './not-found404/not-found404.component';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    component: AuthWrapperComponent,
    children: [
      { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'users', redirectTo: '/404' },
      { path: 'my-account', redirectTo: '/404' },
      {
        path: 'homepage',
        loadChildren: () =>
          import('./homepage/homepage.module').then((m) => m.HomepageModule),
      },
      { path: 'blog', redirectTo: '/404' },
      { path: 'api', redirectTo: '/404' },
      { path: 'analytics', redirectTo: '/404' },
      { path: 'settings', redirectTo: '/404' },
      { path: '404', component: NotFound404Component, pathMatch: 'full' },
    ],
  },
  //{ path: 'loadPermData', component: FetchingPermComponent },
  { path: '403', component: NoPermission403Component, pathMatch: 'full' },
  { path: '**', redirectTo: '/404' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
