import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

import { encode as b64Encode } from 'base-64';
import { AuthService } from './auth.service';
import { catchError, map } from 'rxjs/operators';
import jwtDecode from 'jwt-decode';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService) {}

  canActivate():
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (!this.authService.isLoggedIn()) {
      return new Observable((observer) => {
        this.authService
          .fetchAccessToken()
          .pipe(
            catchError((err) => {
              observer.next(false);
              const loginUrl = `${environment.loginUrl}?callbackUrl=${b64Encode(
                environment.loginCallbackUrl
              )}`;
              window.location.href = loginUrl;
              throw err;
            })
          )
          .subscribe((value: any) => {
            if (value.token) {
              this.authService.user = jwtDecode(value.token) as any;
              this.authService.accessToken = value.token;
              observer.next(true);
            } else {
              observer.next(false);
              const loginUrl = `${environment.loginUrl}?callbackUrl=${b64Encode(
                environment.loginCallbackUrl
              )}`;
              window.location.href = loginUrl;
            }
          });
      });
    }
    return true;
  }
}
