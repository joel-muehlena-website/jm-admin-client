import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { SidebarControlEvent } from '../header/header.component';
import { Layout } from '../layout.service';
import { MainNavigationEntrySection } from '../main-navigation-entry';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.sass'],
})
export class SidebarComponent {
  @ViewChild('sidebarRef') sidebarRef!: ElementRef;

  @Input() layout: Layout = 'Desktop';
  @Input() navigationEntries: Array<MainNavigationEntrySection> = [];

  _sidebarControlEvent: SidebarControlEvent | null = null;
  @Input()
  set sidebarControlEvent(event: SidebarControlEvent | null) {
    this._sidebarControlEvent = event;
    this.handleEvent();
  }
  get sidebarControlEvent() {
    return this._sidebarControlEvent;
  }

  private handleEvent() {
    const e = this._sidebarControlEvent;
    if (e === null) return;

    if (e.layout === 'Mobile' && e.type === 'open') {
      if (
        !this.sidebarRef.nativeElement.classList.contains(
          'jmAdminClient__sidebar--expanded'
        )
      ) {
        this.sidebarRef.nativeElement.classList.add(
          'jmAdminClient__sidebar--expanded'
        );
      }

      if (e.state) {
        this.sidebarRef.nativeElement.classList.add('opened');
      } else {
        this.sidebarRef.nativeElement.classList.remove('opened');
      }
    } else {
      if (e.state) {
        this.sidebarRef.nativeElement.classList.add(
          'jmAdminClient__sidebar--expanded'
        );
      } else {
        this.sidebarRef.nativeElement.classList.remove(
          'jmAdminClient__sidebar--expanded'
        );
      }
    }
  }

  closeSidebar() {
    const event: SidebarControlEvent = {
      layout: this.layout,
      state: false,
      type: 'open',
    };
    this.sidebarControlEvent = event;
  }
}
