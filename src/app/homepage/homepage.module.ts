import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about/about.component';
import { RouterModule, Routes } from '@angular/router';
import { RoleGuard } from '../role.guard';

import { MarkdownModule } from 'ngx-markdown';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  {
    path: 'about',
    canActivate: [RoleGuard],
    data: { requiredPermission: 'jmWeb:about:post' },
    component: AboutComponent,
  },
];

@NgModule({
  declarations: [AboutComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MarkdownModule.forChild(),
    FormsModule,
  ],
  providers: [HttpClientModule],
})
export class HomepageModule {}
