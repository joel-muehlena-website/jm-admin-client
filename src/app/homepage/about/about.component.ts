import { Component, OnInit } from '@angular/core';
import { AboutService } from '../about.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.sass'],
})
export class AboutComponent implements OnInit {
  aboutText: string | null = null;
  private aboutTextNoEdit: string | null = null;
  isSendSuccessful: boolean = false;

  constructor(private aboutService: AboutService) {}

  ngOnInit(): void {
    this.getAbout();
  }

  getAbout() {
    this.aboutService
      .getAbout()
      .subscribe((data) => (this.aboutText = this.aboutTextNoEdit = data.data));
  }

  onReset() {
    this.aboutText = this.aboutTextNoEdit;
  }

  deactivateSuccessMessage(_: KeyboardEvent) {
    this.isSendSuccessful = false;
  }

  onSave() {
    if (this.aboutText !== null && this.aboutText !== this.aboutTextNoEdit) {
      this.aboutService.setAbout(this.aboutText).subscribe(() => {
        this.isSendSuccessful = true;
      });
    }
  }
}
