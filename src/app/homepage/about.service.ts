import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AboutService {
  constructor(private http: HttpClient) {}

  getAbout(): Observable<{ data: string }> {
    return this.http.get<{ data: string }>(`${environment.apiUrl}/v1/about/`);
  }

  setAbout(data: string) {
    return this.http.post(`${environment.apiUrl}/v1/about/`, { text: data });
  }
}
