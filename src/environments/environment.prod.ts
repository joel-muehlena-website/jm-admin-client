/**
 * Holds the config vars for the prod environment
 */
export const environment = {
  production: true,
  authServer: 'https://auth.joel.muehlena.de',
  loginUrl: 'https://joel.muehlena.de/login',
  loginCallbackUrl: 'https://admin.joel.muehlena.de',
  apiUrl: 'https://api.joel.muehlena.de',
};
